import { Component } from "react";


class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hour: new Date().getHours(),
            minute: new Date().getMinutes(),
            second: new Date().getSeconds()
        }
    }
    onPageLoading = () => {
        window.location.reload();
        //    if (this.state.second % 2 === 0) {
        //         console.log("so chan")
        //    }
    }
    render() {
        let newformat = this.state.hour >= 12 ? 'PM' : 'AM';
        let hours = this.state.hour % 12;

        // To display "0" as "12"
        hours = hours < 10 ? "0" + hours : hours;
        return (
            <>
                {this.state.second % 2 === 0 ? <h1 className="text-danger">Hello, world!</h1> : <h1 className="text-success">Hello, world!</h1>}

                {this.state.second % 2 === 0 ?  <h3 className="text-danger">It is {hours}:{this.state.minute}:{this.state.second} {newformat}</h3> :  <h3 className="text-success">It is {hours}:{this.state.minute}:{this.state.second} {newformat}</h3>}

               
                <button onClick={this.onPageLoading}>Change Color</button>


            </>
        )
    }
}
export default Content