import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Content from './components/Content';


function App() {
  return (
    <div>
      <Content></Content>
    </div>
  );
}

export default App;
